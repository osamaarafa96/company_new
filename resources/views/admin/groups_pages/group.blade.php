<?php if(isset($page) && !empty($page) && $page!=null ):
    $out['form']='dashC/'.$page->id;
    $out['page_id']=$page->page_id;
    $out['page_title']=$page->page_title;
    $out['page_link']=$page->page_link;
    $out['page_order']=$page->page_order;
    $out['page_icon_code']=$page->page_icon_code;
    $out['page_image']=$page->page_image;
    $out['method_input']='<input name="_method" value="PUT" type="hidden" />';
    $out['input']='<input type="submit" name="edit_groupe" value="تعديل" class="btn center-block" /> ';
else:
    $out['form']='dashC';
    $out['page_id']="";
    $out['method_input']='';
    $out['page_title']="";
    $out['page_link']="#";
    $out['page_order']="";
    $out['page_icon_code']="";
    $out['page_image']="";
    $out['input']='<input type="submit" name="add_groupe" value="إضافة" class="btn center-block btn-success" />';
endif?>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="header-icon">
            <i class="fa fa-building-o"></i>
        </div>
        <div class="header-title">
            <h1 style=" line-height: 1.7;">{{$title}}</h1>

        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="btn-group" id="buttonexport">
                            <a href="#">
                                <h4>{{$title}}</h4>
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">

                        <form action="{{asset($out['form'])}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                             {!! $out['method_input'] !!}
                            <div class="col-sm-12">
                                <div class="col-md-6 form-group">
                                    <label class="control-label">إسم المجموعة</label>
                                    <input type="text" name="page_title" placeholder="إسم المجموعة" value="<?php echo $out['page_title']; ?>" required class="form-control">
                                </div>

                                <div class="col-md-6 form-group">
                                    <label class="control-label">لوجو المجموعة</label>

                                    {!! Form::file('page_image', ['class'=>"form-control",'placeholder'=>"إسم المجموعة"]) !!}
                                </div>

                            </div>

                            <div class="col-sm-12">
                                <div class="col-md-6 form-group">
                                    <label class="control-label">رابط المجموعة</label>
                                    <input type="text" name="page_link" placeholder="إسم المجموعة" value="<?php echo $out['page_link']; ?>" required class="form-control">
                                </div>

                                <div class="col-md-6 form-group">
                                    <label class="control-label">إختر أيقونة</label>
                                    <select  name="page_icon_code" class=" form-control selectpicker"  data-validation="required" aria-required="true" data-show-subtext="true" data-live-search="true">
                                        <option value=""> إختر الأيقونة  </option>
                                        <?php foreach ($font_icon as $icone):
                                        $select=""; if(isset($result_id) ){
                                            $ceck=explode("fa ",$out['page_icon_code']);
                                            if($icone->name ==$ceck[1]  ){$select='selected="selected"';}
                                        } ?>
                                        <option  value="fa <?php echo $icone->name?>" <?php echo  $select?>   data-content="<i class='fa <?php echo $icone->name?>' aria-hidden='true'></i><?php echo $icone->name?>"></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>

                                <?php if(isset($out['page_image']) && !empty($out['page_image']) && $out['page_image']!=null ): ?>
                                <div class="col-xs-12">
                                    <div class="col-xs-6">
                                        <img src="{{asset("images/$page->page_image")}}"  style="width: 300px;height: 300px"/>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>




                            <div class="col-sm-12">
                                <div class="col-md-5 col-sm-3 col-xs-6 inner-details-btn"></div>
                                <div class="col-md-3 col-sm-3 col-xs-6 inner-details-btn">
                                    <?php echo $out['input']?>
                                </div>
                                <div class="col-md-4 col-sm-3 col-xs-6 inner-details-btn"></div>
                            </div>
                            <br>
                            <br>
                        </form>







                        <?php  if(isset($Page_table ) && $Page_table!=null && !empty($Page_table)):?>
                        <table id="" class="table table-bordered table-striped table-hover example" cellspacing="0" width="100%">
                            <caption class="text-right text-success"></caption>
                            <thead>
                            <tr class="">
                                <th width="">م</th>
                                <th width="" class="">العنوان</th>
                                <th width="" class="">الرابط</th>
                                <th width="" class="">الترتيب</th>
                                <th width="" class="">الايقونة</th>
                                <th> لوجو المجموعة</th>
                                <th width="" class="">التحكم</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $x = 0;
                            foreach($Page_table as $group):
                            $x++;
                            ?>
                            <tr>
                                <td data-title="م"><span class="badge"><?php echo $x?></span></td>
                                <td data-title="العنوان"> <?php echo $group->page_title?> </td>
                                <td data-title="الرابط"><?php echo $group->page_link?></td>
                                <td data-title="الترتيب"><?php echo $group->page_order?></td>
                                <td data-title="الايقونة" ><i class="<?php echo $group->page_icon_code.' fa-2x'?>"></i></td>

                                <?php if(!empty($group->page_image)  && $group->page_image !="0"){
                                    $image= '<img src="'. asset("images/$group->page_image").'" style="width: 100px;height: 70px">';
                                }else{ $image="لم يتم الاضافة "; }
                                ?>

                                <td data-title="لوجو المجموعة"><?php echo $image ?> </td>

                                <td data-title="التحكم" class="text-center">
                                    <a href="{{asset("dashC/$group->id/edit")}}" class="btn btn-success btn-sm">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete{{$group->id}}"><i class="fa fa-trash-o"></i> </button>
                            </tr>
                            <?php endforeach ;?>
                            </tbody>
                        </table>
                        @foreach($Page_table as $group)
                            <div class="modal fade" id="delete{{$group->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header modal-header-primary">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h3><i class="fa fa-building m-r-5"></i>  تنبيه</h3>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form method="post" action="{{asset('dashC/'.$group->id)}}" class="form-horizontal">
                                                        <fieldset>
                                                            <div class="col-md-12 form-group user-form-group">
                                                                <label class="control-label">تاكيد اجراء الحذف؟</label>
                                                                <div class="pull-right">
                                                                    {{csrf_field()}}
                                                                    <input name='_method' value='DELETE' type="hidden" />
                                                                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">لا</button>
                                                                    <button type="submit" class="btn btn-success btn-sm">نعم</button>
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" style="margin-right: 520px">غلق</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        @endforeach



                        <?php endif;?>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>








