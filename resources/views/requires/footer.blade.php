<footer class="main-footer">
   <strong>Copyright &copy; 2016-2017 <a href="#">thememinister</a>.</strong> All rights reserved.
</footer>
</div>


<!-- ./wrapper -->
<!-- Start Core Plugins
=====================================================================-->
<!-- jQuery -->
<script src="{{ asset('plugins/jQuery/jquery-1.12.4.min.js') }}" type="text/javascript"></script>
<!-- jquery-ui -->
<script src="{{ asset('plugins/jquery-ui-1.12.1/jquery-ui.min.js') }}" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- lobipanel -->
<script src="{{ asset('plugins/lobipanel/lobipanel.min.js') }}" type="text/javascript"></script>
<!-- Pace js -->
<script src="{{ asset('plugins/pace/pace.min.js') }}" type="text/javascript"></script>
<!-- table-export js -->
<script src="{{ asset('plugins/table-export/tableExport.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/table-export/jquery.base64.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/table-export/html2canvas.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/table-export/sprintf.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/table-export/jspdf.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/table-export/base64.js') }}" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">

<!-- dataTables js -->
<script src="{{ asset('plugins/datatables/dataTables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin_asset/js/tables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin_asset/js/tables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin_asset/js/tables/buttons.flash.min.js') }}"></script>
<script src="{{ asset('admin_asset/js/tables/jszip.min.js') }}"></script>
<script src="{{ asset('admin_asset/js/tables/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin_asset/js/tables/vfs_fonts.js') }}"></script>
<script src="{{ asset('admin_asset/js/tables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin_asset/js/tables/buttons.print.min.js') }}"></script>
<script src="{{ asset('admin_asset/js/tables/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('admin_asset/js/tables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin_asset/js/tables/plugin.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('admin_asset/plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<!-- FastClick -->
<script src="{{ asset('admin_asset/plugins/fastclick/fastclick.min.js') }}" type="text/javascript"></script>
<!-- CRMadmin frame -->
<script src="{{ asset('admin_asset/js/custom.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin_asset/js/tables/plugin.js') }}" type="text/javascript"></script>
<!-- End Core Plugins
=====================================================================-->
<!-- Start Theme label Script
=====================================================================-->
<!-- Dashboard js -->
<script src="{{ asset('admin_asset/js/dashboard.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin_asset/js/bootstrap-select.min.js') }}"></script>
<script type="text/javascript">
   $('.selectpicker').selectpicker("refresh");
</script>
<!-- End Theme label Script
=====================================================================-->
</body>

<!-- Mirrored from crm.thememinister.com/crmAdmin/balance.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 13 Jun 2017 08:06:11 GMT -->

