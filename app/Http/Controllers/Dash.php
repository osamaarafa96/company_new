<?php

namespace App\Http\Controllers;




use App\System_management\Page;
use Illuminate\Http\Request;

class Dash extends Controller
{
    public function __construct()
    {
        $this->middleware('IsAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function index()
    {
        //
        $data["font_icon"]=Page::get();
        $data["Page_table"] = Page::where('group_id_fk',0)->get();
        $data['title'] = 'إضافة مجموعة تحكم';
        $data['metakeyword'] = 'اعداددات الموقع ';
        $data['subView'] = 'admin.groups_pages.group';
//        var_dump($data["Page_table"]);
//
        return view('master_page', $data);
    }


    public  function GroupsPages(Request $request)
    {
        $input = $request->all();
        if($file = $request->file('file')){
            $name = $file->getClientOriginalName();
            $file->move('images', $name);

            $input['page_image'] = $name;
        }
        Page::create($input);

        //  $this->message('success','تمت إضافة المجموعة بنجاح');
            redirect('dashC', 'refresh');
        
        

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();
        if($file = $request->file('page_image')){


            $name = $file->getClientOriginalName();
            $file->move('images', $name);

            $input['page_image'] = $name;
        }
        Page::create($input);

        //  $this->message('success','تمت إضافة المجموعة بنجاح');
        return redirect('/dashC');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data["font_icon"]=Page::get();
//        $data["Page_table"] = Page::where('group_id_fk',0)->get();
        $data['title'] = 'إضافة مجموعة تحكم';
        $data['page'] = Page::findOrfail($id);
        $data['subView'] = 'admin.groups_pages.group';

        return view('master_page', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $page = Page::findOrFail($id);
        if($file = $request->file('page_image')){


            $name = $file->getClientOriginalName();
            $file->move('images', $name);

            $input['page_image'] = $name;
        }

        $page->update($request->all());

        return redirect('/dashC');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Page::whereId($id)->delete();

        return redirect('/dashC');
    }


    /**
     * @param Request $request
     */
    public function subPages(Request $request)
    {
        if($request->input('add_page')){
            echo"klkm";
        }
        $data['pages']=Page::where('group_id_fk','!=',0)->get();;
        $data['pages_name']=Page::all(['id','page_title']);
        echo "<pre>";
        print_r($data['pages_name'] );
        echo "</pre>";

        die;
        $data["font_icon"]=$this->Difined_model->select_all("font_icons","id","","id","ASC");
        $data["groups"]=$this->Groups->level_groups();

    }
}
