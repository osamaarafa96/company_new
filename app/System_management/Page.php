<?php

namespace App\System_management;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    //
    protected $fillable = [
        'page_title',
        'page_link',
        'page_icon_code',
        'page_image'
    ];

    public static function where($string, $string1, $int)
    {
    }
}
